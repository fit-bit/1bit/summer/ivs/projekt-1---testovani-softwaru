//======== Copyright (c) 2019, FIT VUT Brno, All rights reserved. ============//
//
// Purpose:     Test Driven Development - priority queue code
//
// $NoKeywords: $ivs_project_1 $tdd_code.cpp
// $Author:     Matej Kudera <xkuder04@stud.fit.vutbr.cz>
// $Date:       $2019-02-15
//============================================================================//
/**
 * @file tdd_code.cpp
 * @author Matej Kudera
 * 
 * @brief Implementace metod tridy prioritni fronty.
 */

#include <stdlib.h>
#include <stdio.h>

#include "tdd_code.h"

//============================================================================//
// ** ZDE DOPLNTE IMPLEMENTACI **
//
// Zde doplnte implementaci verejneho rozhrani prioritni fronty (Priority Queue)
// 1. Verejne rozhrani fronty specifikovane v: tdd_code.h (sekce "public:")
//    - Konstruktor (PriorityQueue()), Destruktor (~PriorityQueue())
//    - Metody Insert/Remove/Find a GetHead
//    - Pripadne vase metody definovane v tdd_code.h (sekce "protected:")
//
// Cilem je dosahnout plne funkcni implementace prioritni fronty implementovane
// pomoci tzv. "double-linked list", ktera bude splnovat dodane testy 
// (tdd_tests.cpp).
//============================================================================//

/**
Summary coverage rate:
  lines......: 100.0% (118 of 118 lines)
  functions..: 95.3% (41 of 43 functions)
  branches...: no data found 
**/

PriorityQueue::PriorityQueue()
{
	m_pHead = NULL;
}

PriorityQueue::~PriorityQueue()
{
	// potřebná proměnná
        struct Element_t *currentItem = m_pHead;

	// postupně ulovní všechny prvky
	while (currentItem != NULL)
	{
		// když už je to poslední prvek, tak se uvolní sám.
		if (currentItem->pNext == NULL)
		{
			free(currentItem);
			m_pHead = NULL;
			return;
		}
		else
		{
			currentItem = currentItem->pNext;
			free(currentItem->pPrev);
		}
	}
}

void PriorityQueue::Insert(int value)
{
	// potřebné proměnné
	struct Element_t *currentItem = m_pHead;
	struct Element_t *newItem = (struct Element_t*) malloc(sizeof(struct Element_t));

	if (newItem != NULL)
	{
		if (currentItem == NULL)
		{
			m_pHead = newItem;
			newItem->pNext = NULL;
			newItem->pPrev = NULL;
			newItem->value = value;
		}
		else
		{
			// když je element potřeba někam doplnit
			while(currentItem != NULL)
			{
				if (currentItem->value >= value)
				{
					newItem->pNext = currentItem;
					newItem->pPrev = currentItem->pPrev;
					currentItem->pPrev = newItem;

					//když se to má dát na začátek
					if (m_pHead == currentItem)
					{
						m_pHead = newItem;
					}
					else
					{
						newItem->pPrev->pNext = newItem;
					}

					newItem->value = value;
					return;
				}

				// když je element největší a patří nakonec
				if (currentItem->pNext == NULL)
				{
					currentItem->pNext = newItem;
					newItem->pPrev = currentItem;
					newItem->pNext = NULL;
					newItem->value = value;
					return;
				}

				currentItem = currentItem->pNext;
			}

		}
	}
}

bool PriorityQueue::Remove(int value)
{
	// potřebná proměnná
	struct Element_t *currentItem = m_pHead;

	// najití prvku
	while(currentItem != NULL)
	{
		// přehození ukazatelů když najdeme prvek
		if (currentItem->value == value)
		{
			// když se maže jediný prvek v seznamu
			if (currentItem->pNext == NULL && currentItem->pPrev == NULL)
			{
				m_pHead = NULL;
			}
			else
			{
				// když se maže poslední prvek
				if (currentItem->pNext == NULL)
				{
					currentItem->pPrev->pNext = NULL;
				}
				else
				{
					currentItem->pNext->pPrev = currentItem->pPrev;
				}

				// když se maže první prvek
				if (currentItem->pPrev == NULL)
				{
					m_pHead = currentItem->pNext;
				}
				else
				{
					currentItem->pPrev->pNext = currentItem->pNext;
				}
			}

			free(currentItem);
			return true;
		}
		
		currentItem = currentItem->pNext;	
	}

	return false;
}

PriorityQueue::Element_t *PriorityQueue::Find(int value)
{
	// potřebná proměnná
	struct Element_t *currentItem = m_pHead;

	// najití prvku
	while(currentItem != NULL)
	{
		// když se prvek najde prátí se ukazatel
		if (currentItem->value == value)
		{
			return currentItem;
		}
		
		currentItem = currentItem->pNext;	
	}

	return NULL;
}

PriorityQueue::Element_t *PriorityQueue::GetHead()
{
    return m_pHead;
}

/*** Konec souboru tdd_code.cpp ***/
