//======== Copyright (c) 2019, FIT VUT Brno, All rights reserved. ============//
//
// Purpose:     White Box - Tests suite
//
// $NoKeywords: $ivs_project_1 $white_box_code.cpp
// $Author:     Matej Kudera <xkuder04@stud.fit.vutbr.cz>
// $Date:       $2019-02-15
//============================================================================//
/**
 * @file white_box_tests.cpp
 * @author Matej Kudera
 * 
 * @brief Implementace testu prace s maticemi.
 */

#include "gtest/gtest.h"
#include "white_box_code.h"

//============================================================================//
// ** ZDE DOPLNTE TESTY **
//
// Zde doplnte testy operaci nad maticemi. Cilem testovani je:
// 1. Dosahnout maximalniho pokryti kodu (white_box_code.cpp) testy.
// 2. Overit spravne chovani operaci nad maticemi v zavislosti na rozmerech 
//    matic.
//============================================================================//

/**
Summary coverage rate:
  lines......: 99.7% (714 of 716 lines)
  functions..: 100.0% (123 of 123 functions)
  branches...: no data found 
**/

// One x One
TEST(MatrixOnexOne, SetOne)
{
	// Udělání matice
	Matrix test;

	EXPECT_TRUE(test.get(0,0) == 0.0);

	EXPECT_TRUE(test.set(0,0,1));
	EXPECT_TRUE(test.get(0,0) == 1.0);
	EXPECT_TRUE(test.set(0,0,-2));
        EXPECT_TRUE(test.get(0,0) == -2.0);
	EXPECT_TRUE(test.set(0,0,2.5));
	EXPECT_TRUE(test.get(0,0) == 2.5);
	EXPECT_TRUE(test.set(0.5,0.5,3.6));
	EXPECT_TRUE(test.get(0,0) == 3.6);

	EXPECT_FALSE(test.set(-1,-1,1));
        EXPECT_FALSE(test.set(1,1,2.5));
	EXPECT_FALSE(test.set(1.1,1.1,2));
}

TEST(MatrixOnexOne, SetFromArray)
{
	// udělání pole
	std::vector<double> item;
	item.push_back(1);
	std::vector<std::vector<double> > array;
	array.push_back(item);

	// Udělání matice
        Matrix test;

	// test jedna hodnotota
	EXPECT_TRUE(test.get(0,0) == 0.0);

	EXPECT_TRUE(test.set(array));
	EXPECT_TRUE(test.get(0,0) == 1.0);

	// test druhá hodnota
	array.pop_back();
	item.pop_back();
	item.push_back(2.6);
	array.push_back(item);
	
	EXPECT_TRUE(test.set(array));
        EXPECT_TRUE(test.get(0,0) == 2.6);

	// zkouška většího pole
	item.pop_back();
	item.push_back(1);
	array.push_back(item);

	EXPECT_FALSE(test.set(array));
	EXPECT_TRUE(test.get(0,0) == 2.6);

	// zkouška ještě většího pole
	item.pop_back();
        item.push_back(1);
	array.push_back(item);

	EXPECT_FALSE(test.set(array));
        EXPECT_TRUE(test.get(0,0) == 2.6);

	// zkouška prázdného pole
	array.pop_back();
	array.pop_back();
	array.pop_back();

	EXPECT_FALSE(test.set(array));
	EXPECT_TRUE(test.get(0,0) == 2.6);
}

TEST(MatrixOnexOne, Get)
{
	// Udělání matice
	Matrix test;

	EXPECT_TRUE(test.get(0,0) == 0.0);
	EXPECT_ANY_THROW(test.get(-1,-1));
	EXPECT_ANY_THROW(test.get(1,1));
	
	EXPECT_TRUE(test.set(0,0,1));
	EXPECT_TRUE(test.get(0,0) == 1.0);
}

TEST(MatrixOnexOne, OpEqual)
{
	// Udělání matic
        Matrix mat1, mat2;
	Matrix notOne(2,2);

	EXPECT_TRUE(mat1.set(0,0,1));
	EXPECT_TRUE(mat2.set(0,0,1));

	// Jedna hodnota
	EXPECT_TRUE(mat1.operator==(mat2));
	EXPECT_TRUE(mat2.operator==(mat1));

	// Druhá hodnota
	EXPECT_TRUE(mat1.set(0,0,2.5));
	EXPECT_FALSE(mat1.operator==(mat2));
	EXPECT_FALSE(mat2.operator==(mat1));

	// Rozdílné matice
	EXPECT_ANY_THROW(mat1.operator==(notOne));
	EXPECT_ANY_THROW(notOne.operator==(mat1));
}

TEST(MatrixOnexOne, OpPlus)
{
	// Udělání matic
        Matrix mat1, mat2, plus;
	Matrix notOne(2,2);

        EXPECT_TRUE(mat1.set(0,0,1));
        EXPECT_TRUE(mat2.set(0,0,1));
	
	// Jedna hodnota
	EXPECT_NO_THROW(plus = mat1.operator+(mat2));
	EXPECT_TRUE(plus.get(0,0) == 2.0);
	EXPECT_NO_THROW(plus = mat2.operator+(mat1));
        EXPECT_TRUE(plus.get(0,0) == 2.0);

	// Druhá hodnota
	EXPECT_TRUE(mat1.set(0,0,-2.6));
	EXPECT_NO_THROW(plus = mat1.operator+(mat2));
        EXPECT_TRUE(plus.get(0,0) == -1.6);
	EXPECT_NO_THROW(plus = mat2.operator+(mat1));
        EXPECT_TRUE(plus.get(0,0) == -1.6);

	// Sečtení jiných matic
	EXPECT_ANY_THROW(plus = mat1.operator+(notOne));
	EXPECT_ANY_THROW(plus = notOne.operator+(mat1));
}

TEST(MatrixOnexOne, MatrixxMatrix)
{
	// Udělání matic
        Matrix mat1, mat2, multiplication;
        Matrix notOne(2,2);

        EXPECT_TRUE(mat1.set(0,0,1));
        EXPECT_TRUE(mat2.set(0,0,1));

	// Jedna hodnota
	EXPECT_NO_THROW(multiplication = mat1.operator*(mat2));
        EXPECT_TRUE(multiplication.get(0,0) == 1.0);
	EXPECT_NO_THROW(multiplication = mat2.operator*(mat1));
        EXPECT_TRUE(multiplication.get(0,0) == 1.0);

	// Druhá hodnota
	EXPECT_TRUE(mat1.set(0,0,-2.6));
        EXPECT_NO_THROW(multiplication = mat1.operator*(mat2));
        EXPECT_TRUE(multiplication.get(0,0) == -2.6);
	EXPECT_NO_THROW(multiplication = mat2.operator*(mat1));
        EXPECT_TRUE(multiplication.get(0,0) == -2.6);

	// vynásobení jiných matic
	EXPECT_ANY_THROW(multiplication = mat1.operator*(notOne));
	EXPECT_ANY_THROW(multiplication = notOne.operator*(mat1));

}

TEST(MatrixOnexOne, MatrixxValue)
{
	// Udělání matic
        Matrix mat1, multiplication;

        EXPECT_TRUE(mat1.set(0,0,1));

	// Jedna hodnota
        multiplication = mat1.operator*(2.1);
        EXPECT_TRUE(multiplication.get(0,0) == 2.1);

	// Druhá hodnota
        multiplication = mat1.operator*(0);
        EXPECT_TRUE(multiplication.get(0,0) == 0.0);
}

TEST(MatrixOnexOne, SolveEquation)
{
	// Udělání matice a vektoru
        Matrix mat1;
	std::vector<double> rightSide, result;

	// Nula v matici
	
	// test s prázdným vektorem
	EXPECT_ANY_THROW(result = mat1.solveEquation(rightSide));

	// test s dobrým vektorem ale není 0
	rightSide.push_back(1);
	EXPECT_ANY_THROW(result = mat1.solveEquation(rightSide));

	// test s vetším vektorem
	rightSide.push_back(2);
	EXPECT_ANY_THROW(result = mat1.solveEquation(rightSide));

	// test s vektorem s 0
	rightSide.pop_back();
	rightSide.pop_back();
	rightSide.push_back(0);
	EXPECT_ANY_THROW(result = mat1.solveEquation(rightSide));

	// ne nula v matici
	EXPECT_TRUE(mat1.set(0,0,2));

	// test s prázdným vektorem 
	rightSide.pop_back();
	EXPECT_ANY_THROW(result = mat1.solveEquation(rightSide));

	// test s dobrým vektorem ale není 0
        rightSide.push_back(1);
        EXPECT_NO_THROW(result = mat1.solveEquation(rightSide));
	EXPECT_TRUE(result.size() == 1);
	EXPECT_TRUE(result[0] == 0.5);

	// test s vetším vektorem
	rightSide.push_back(2); 
        EXPECT_ANY_THROW(result = mat1.solveEquation(rightSide));

	// test s vektorem s 0
        rightSide.pop_back();
        rightSide.pop_back();
        rightSide.push_back(0);
        EXPECT_NO_THROW(result = mat1.solveEquation(rightSide));
	EXPECT_TRUE(result.size() == 1);
        EXPECT_TRUE(result[0] == 0.0);
}

// Two x Two
TEST(MatrixTwoxTwo, SetOne)
{
	// Udělání matice
	Matrix test(2,2);

	EXPECT_TRUE(test.get(0,0) == 0.0);
	EXPECT_TRUE(test.get(0,1) == 0.0);
	EXPECT_TRUE(test.get(1,0) == 0.0);
	EXPECT_TRUE(test.get(1,1) == 0.0);

	EXPECT_TRUE(test.set(0,0,1));
	EXPECT_TRUE(test.get(0,0) == 1.0);
	EXPECT_TRUE(test.set(0,1,-2));
        EXPECT_TRUE(test.get(0,1) == -2.0);
	EXPECT_TRUE(test.set(1,1,2.5));
	EXPECT_TRUE(test.get(1,1) == 2.5);
	EXPECT_TRUE(test.set(1.5,0.5,3.6));
	EXPECT_TRUE(test.get(1,0) == 3.6);

	EXPECT_FALSE(test.set(-2,-2,1));
        EXPECT_FALSE(test.set(1.1,6,2.5));
	EXPECT_FALSE(test.set(0,-3,2));
}

TEST(MatrixTwoxTwo, SetFromArray)
{
	// udělání pole
	std::vector<double> item;
	std::vector<std::vector<double> > array;
	item.push_back(1);
	item.push_back(1);
	array.push_back(item);
	item.pop_back();
	item.pop_back();
        item.push_back(2.5);
	item.push_back(4);
        array.push_back(item);

	// Udělání matice
        Matrix test(2,2);

	// test jedna hodnotota
	EXPECT_TRUE(test.get(0,0) == 0.0);
	EXPECT_TRUE(test.get(1,0) == 0.0);
	EXPECT_TRUE(test.get(0,1) == 0.0);
	EXPECT_TRUE(test.get(1,1) == 0.0);

	EXPECT_TRUE(test.set(array));
	EXPECT_TRUE(test.get(0,0) == 1.0);
	EXPECT_TRUE(test.get(0,1) == 1.0);
	EXPECT_TRUE(test.get(1,0) == 2.5);
	EXPECT_TRUE(test.get(1,1) == 4);

	// test druhá hodnota
	array.pop_back();
	array.pop_back();
	item.pop_back();
	item.pop_back();
	item.push_back(2.6);
	item.push_back(-2);
	array.push_back(item);
        item.pop_back();
	item.pop_back();
        item.push_back(0);
	item.push_back(3.6);
        array.push_back(item);
	
	EXPECT_TRUE(test.set(array));
        EXPECT_TRUE(test.get(0,0) == 2.6);
	EXPECT_TRUE(test.get(0,1) == -2);
        EXPECT_TRUE(test.get(1,0) == 0);
        EXPECT_TRUE(test.get(1,1) == 3.6);

	// zkouška většího pole
	item.pop_back();
	item.push_back(1);
	array.push_back(item);

	EXPECT_FALSE(test.set(array));
	EXPECT_TRUE(test.get(0,0) == 2.6);
        EXPECT_TRUE(test.get(0,1) == -2);
        EXPECT_TRUE(test.get(1,0) == 0);
        EXPECT_TRUE(test.get(1,1) == 3.6);

	// zkouška ještě většího pole
	item.pop_back();
        item.push_back(1);
	array.push_back(item);

	EXPECT_FALSE(test.set(array));
        EXPECT_TRUE(test.get(0,0) == 2.6);
        EXPECT_TRUE(test.get(0,1) == -2);
        EXPECT_TRUE(test.get(1,0) == 0);
        EXPECT_TRUE(test.get(1,1) == 3.6);

	// zkouška prázdného pole
	array.pop_back();
	array.pop_back();
        array.pop_back();
        array.pop_back();

	EXPECT_FALSE(test.set(array));
	EXPECT_TRUE(test.get(0,0) == 2.6);
        EXPECT_TRUE(test.get(0,1) == -2);
        EXPECT_TRUE(test.get(1,0) == 0);
        EXPECT_TRUE(test.get(1,1) == 3.6);
}

TEST(MatrixTwoxTwo, Get)
{
	// Udělání matice
	Matrix test(2,2);

	EXPECT_TRUE(test.get(0,0) == 0.0);
	EXPECT_ANY_THROW(test.get(-1,-1));
	EXPECT_ANY_THROW(test.get(3.5,2));
	
	EXPECT_TRUE(test.set(0,0,1));
	EXPECT_TRUE(test.get(0,0) == 1.0);
}

TEST(MatrixTwoxTwo, OpEqual)
{
	// Udělání matic
        Matrix mat1(2,2), mat2(2,2);
	Matrix notOne(1,2);

	// Vyplnění matic
	EXPECT_TRUE(mat1.set(0,0,1));
        EXPECT_TRUE(mat1.set(0,1,-2));
        EXPECT_TRUE(mat1.set(1,0,2.5));
        EXPECT_TRUE(mat1.set(1,1,3.6));

	EXPECT_TRUE(mat2.set(0,0,1));
        EXPECT_TRUE(mat2.set(0,1,-2));
        EXPECT_TRUE(mat2.set(1,0,2.5));
        EXPECT_TRUE(mat2.set(1,1,3.6));

	// Jedna hodnota
	EXPECT_TRUE(mat1.operator==(mat2));
	EXPECT_TRUE(mat2.operator==(mat1));

	// Druhá hodnota
	EXPECT_TRUE(mat1.set(0,0,2.5));
	EXPECT_FALSE(mat1.operator==(mat2));
	EXPECT_FALSE(mat2.operator==(mat1));

	// Rozdílné matice
	EXPECT_ANY_THROW(mat1.operator==(notOne));
	EXPECT_ANY_THROW(notOne.operator==(mat1));
}

TEST(MatrixTwoxTwo, OpPlus)
{
	// Udělání matic
        Matrix mat1(2,2), mat2(2,2), plus(2,2);
	Matrix notOne(3,3);

	// Vyplnění matic
        EXPECT_TRUE(mat1.set(0,0,1));
        EXPECT_TRUE(mat1.set(0,1,-2));
        EXPECT_TRUE(mat1.set(1,0,2.5));
        EXPECT_TRUE(mat1.set(1,1,3.6));

        EXPECT_TRUE(mat2.set(0,0,1));
        EXPECT_TRUE(mat2.set(0,1,-2));
        EXPECT_TRUE(mat2.set(1,0,2.5));
        EXPECT_TRUE(mat2.set(1,1,3.6));

	// Jedna hodnota
	EXPECT_NO_THROW(plus = mat1.operator+(mat2));
	EXPECT_TRUE(plus.get(0,0) == 2.0);
	EXPECT_TRUE(plus.get(0,1) == -4.0);
	EXPECT_TRUE(plus.get(1,0) == 5.0);
	EXPECT_TRUE(plus.get(1,1) == 7.2);
	EXPECT_NO_THROW(plus = mat2.operator+(mat1));
        EXPECT_TRUE(plus.get(0,0) == 2.0);
        EXPECT_TRUE(plus.get(0,1) == -4.0);
        EXPECT_TRUE(plus.get(1,0) == 5.0);
        EXPECT_TRUE(plus.get(1,1) == 7.2);

	// Druhá hodnota
	EXPECT_TRUE(mat1.set(0,0,-2.6));
	EXPECT_NO_THROW(plus = mat1.operator+(mat2));
        EXPECT_TRUE(plus.get(0,0) == -1.6);
        EXPECT_TRUE(plus.get(0,1) == -4.0);
        EXPECT_TRUE(plus.get(1,0) == 5.0);
        EXPECT_TRUE(plus.get(1,1) == 7.2);
	EXPECT_NO_THROW(plus = mat2.operator+(mat1));
        EXPECT_TRUE(plus.get(0,0) == -1.6);
        EXPECT_TRUE(plus.get(0,1) == -4.0);
        EXPECT_TRUE(plus.get(1,0) == 5.0);
        EXPECT_TRUE(plus.get(1,1) == 7.2);

	// Sečtení jiných matic
	EXPECT_ANY_THROW(plus = mat1.operator+(notOne));
	EXPECT_ANY_THROW(plus = notOne.operator+(mat1));
}

TEST(MatrixTwoxTwo, MatrixxMatrix)
{
	// Udělání matic
        Matrix mat1(2,2), mat2(2,2), multiplication(2,2);
        Matrix notOne(1,3);

        // Vyplnění matic
        EXPECT_TRUE(mat1.set(0,0,1));
        EXPECT_TRUE(mat1.set(0,1,-2));
        EXPECT_TRUE(mat1.set(1,0,2));
        EXPECT_TRUE(mat1.set(1,1,3));

        EXPECT_TRUE(mat2.set(0,0,1));
        EXPECT_TRUE(mat2.set(0,1,-2));
        EXPECT_TRUE(mat2.set(1,0,2));
        EXPECT_TRUE(mat2.set(1,1,-3));

	// Jedna hodnota
	EXPECT_NO_THROW(multiplication = mat1.operator*(mat2));
        EXPECT_TRUE(multiplication.get(0,0) == -3.0);
        EXPECT_TRUE(multiplication.get(0,1) == 4.0);
        EXPECT_TRUE(multiplication.get(1,0) == 8.0);
        EXPECT_TRUE(multiplication.get(1,1) == -13.0);
	EXPECT_NO_THROW(multiplication = mat2.operator*(mat1));
        EXPECT_TRUE(multiplication.get(0,0) == -3.0);
        EXPECT_TRUE(multiplication.get(0,1) == -8.0);
        EXPECT_TRUE(multiplication.get(1,0) == -4.0);
        EXPECT_TRUE(multiplication.get(1,1) == -13.0);

	// Druhá hodnota
	EXPECT_TRUE(mat1.set(1,0,-3));
        EXPECT_NO_THROW(multiplication = mat1.operator*(mat2));
	EXPECT_TRUE(multiplication.get(0,0) == -3.0);
        EXPECT_TRUE(multiplication.get(0,1) == 4.0);
        EXPECT_TRUE(multiplication.get(1,0) == 3.0);
        EXPECT_TRUE(multiplication.get(1,1) == -3.0);
	EXPECT_NO_THROW(multiplication = mat2.operator*(mat1));
	EXPECT_TRUE(multiplication.get(0,0) == 7.0);
        EXPECT_TRUE(multiplication.get(0,1) == -8.0);
        EXPECT_TRUE(multiplication.get(1,0) == 11.0);
        EXPECT_TRUE(multiplication.get(1,1) == -13.0);

	// vynásobení jiných matic
	EXPECT_ANY_THROW(multiplication = mat1.operator*(notOne));
	EXPECT_ANY_THROW(multiplication = notOne.operator*(mat1));

}

TEST(MatrixTwoxTwo, MatrixxValue)
{
	// Udělání matic
        Matrix mat1(2,2), multiplication(2,2);

        EXPECT_TRUE(mat1.set(0,0,1));
        EXPECT_TRUE(mat1.set(0,1,-2));
        EXPECT_TRUE(mat1.set(1,0,2));
        EXPECT_TRUE(mat1.set(1,1,3));

	// Jedna hodnota
        multiplication = mat1.operator*(2.0);
	EXPECT_TRUE(multiplication.get(0,0) == 2.0);
        EXPECT_TRUE(multiplication.get(0,1) == -4.0);
        EXPECT_TRUE(multiplication.get(1,0) == 4.0);
        EXPECT_TRUE(multiplication.get(1,1) == 6.0);

	// Druhá hodnota
        multiplication = mat1.operator*(0);
	EXPECT_TRUE(multiplication.get(0,0) == 0.0);
        EXPECT_TRUE(multiplication.get(0,1) == 0.0);
        EXPECT_TRUE(multiplication.get(1,0) == 0.0);
        EXPECT_TRUE(multiplication.get(1,1) == 0.0);
}

TEST(MatrixTwoxTwo, SolveEquation)
{
	// Udělání matice a vektoru
        Matrix mat1(2,2);
	std::vector<double> rightSide, result;

	// Nuly v matici
	
	// test s prázdným vektorem
	EXPECT_ANY_THROW(result = mat1.solveEquation(rightSide));

	// test s menším vektorem
	rightSide.push_back(1);
	EXPECT_ANY_THROW(result = mat1.solveEquation(rightSide));

	// test s dobrým vektorem ale není 0
        rightSide.push_back(3);
        EXPECT_ANY_THROW(result = mat1.solveEquation(rightSide));

	// test s vetším vektorem
	rightSide.push_back(2);
	EXPECT_ANY_THROW(result = mat1.solveEquation(rightSide));

	// test s vektorem s 0
	rightSide.pop_back();
	rightSide.pop_back();
	rightSide.pop_back();
	rightSide.push_back(0);
	EXPECT_ANY_THROW(result = mat1.solveEquation(rightSide));
	rightSide.push_back(0);
        EXPECT_ANY_THROW(result = mat1.solveEquation(rightSide));

	// test s vektorem ne celý 0
	rightSide.pop_back();
	rightSide.push_back(1);
	EXPECT_ANY_THROW(result = mat1.solveEquation(rightSide));

	rightSide.pop_back();
	rightSide.pop_back();
	rightSide.push_back(1);
	rightSide.push_back(0);
	EXPECT_ANY_THROW(result = mat1.solveEquation(rightSide));

	// jden nulový řádek
	EXPECT_TRUE(mat1.set(0,0,2));

	// test s prázdným vektorem 
	rightSide.pop_back();
	rightSide.pop_back();
	EXPECT_ANY_THROW(result = mat1.solveEquation(rightSide));

	// test s menším vektorem
        rightSide.push_back(1);
        EXPECT_ANY_THROW(result = mat1.solveEquation(rightSide));

	// test s normálním vektorem ale není 0
	rightSide.push_back(2); 
        EXPECT_ANY_THROW(result = mat1.solveEquation(rightSide));

	// test s vetším vektorem
        rightSide.push_back(2);
        EXPECT_ANY_THROW(result = mat1.solveEquation(rightSide));

	EXPECT_TRUE(mat1.set(0,1,1));
	rightSide.pop_back();
	EXPECT_ANY_THROW(result = mat1.solveEquation(rightSide));

	// i druhý řádek
	EXPECT_TRUE(mat1.set(1,1,2));

	EXPECT_NO_THROW(result = mat1.solveEquation(rightSide));
	EXPECT_TRUE(result.size() == 2);
	EXPECT_TRUE(result[0] == 0.0);
	EXPECT_TRUE(result[1] == 1.0);

	EXPECT_TRUE(mat1.set(1,0,4));

	EXPECT_ANY_THROW(result = mat1.solveEquation(rightSide));

	EXPECT_TRUE(mat1.set(1,0,1));
	EXPECT_NO_THROW(result = mat1.solveEquation(rightSide));
        EXPECT_TRUE(result.size() == 2);
        EXPECT_TRUE(result[0] == 0.0);
        EXPECT_TRUE(result[1] == 1.0);

	// test s vektorem s 0
        rightSide.pop_back();
        rightSide.pop_back();
        rightSide.push_back(0);
	rightSide.push_back(0);
        EXPECT_NO_THROW(result = mat1.solveEquation(rightSide));
	EXPECT_TRUE(result.size() == 2);
        EXPECT_TRUE(result[0] == 0.0);
	EXPECT_TRUE(result[1] == 0.0);
}

// Další matice
TEST(Matrix, Constructor)
{
	// Tvorba matic špatných rozměrů
	
	EXPECT_ANY_THROW(Matrix mat1(0,0));
	EXPECT_ANY_THROW(Matrix mat1(0,1));
	EXPECT_ANY_THROW(Matrix mat1(1,0));
	EXPECT_ANY_THROW(Matrix mat1(-1,0));
	EXPECT_ANY_THROW(Matrix mat1(-1,-1));
	EXPECT_ANY_THROW(Matrix mat1(-2,2));
	EXPECT_ANY_THROW(Matrix mat1(0.5,0.5));

	EXPECT_NO_THROW(Matrix mat1(1.5,1.5));
}

TEST(Matrix, OpEqual)
{
	//1x2
	Matrix mat1(1,2), mat2(1,2), mat3(2,1);

	EXPECT_TRUE(mat1.set(0,0,4));
	EXPECT_TRUE(mat1.set(0,1,3));

	EXPECT_TRUE(mat2.set(0,0,4));
        EXPECT_TRUE(mat2.set(0,1,3));

	EXPECT_TRUE(mat3.set(0,0,4));
        EXPECT_TRUE(mat3.set(1,0,3));

	// kontrola rovnosti
	EXPECT_TRUE(mat1.operator==(mat2));
	EXPECT_TRUE(mat2.operator==(mat1));

	EXPECT_TRUE(mat2.set(0,0,5));
	EXPECT_FALSE(mat1.operator==(mat2));
        EXPECT_FALSE(mat2.operator==(mat1));

	// Kontrola vyjímky
	EXPECT_ANY_THROW(mat1.operator==(mat3));
	EXPECT_ANY_THROW(mat2.operator==(mat3));
}

TEST(Matrix, OpPlus)
{
        //1x2
	Matrix mat1(1,2), mat2(1,2), plus(1,2), mat3(2,1);

        EXPECT_TRUE(mat1.set(0,0,4));
        EXPECT_TRUE(mat1.set(0,1,3));

        EXPECT_TRUE(mat2.set(0,0,4));
        EXPECT_TRUE(mat2.set(0,1,3));

        EXPECT_TRUE(mat3.set(0,0,4));
        EXPECT_TRUE(mat3.set(1,0,3));

	// kontrola součtu
	EXPECT_NO_THROW(plus = mat1.operator+(mat2));
	EXPECT_TRUE(plus.get(0,0) == 8.0);
        EXPECT_TRUE(plus.get(0,1) == 6.0);

	// jíná hodnota
	EXPECT_TRUE(mat2.set(0,0,5));
	EXPECT_NO_THROW(plus = mat1.operator+(mat2));
	EXPECT_TRUE(plus.get(0,0) == 9.0);
        EXPECT_TRUE(plus.get(0,1) == 6.0);

	// kontrola vyjímky
	EXPECT_ANY_THROW(plus = mat1.operator+(mat3));
	EXPECT_ANY_THROW(plus = mat2.operator+(mat3));
}

TEST(Matrix, MatrixxValue)
{
        //1x2
	Matrix mat1(1,2), mat2(2,1), multiplication1(1,2), multiplication2(2,1);

	EXPECT_TRUE(mat1.set(0,0,4));
        EXPECT_TRUE(mat1.set(0,1,3));

	EXPECT_TRUE(mat2.set(0,0,4));
        EXPECT_TRUE(mat2.set(1,0,3));

	// kontrola násobení
	/** TODO why?
	multiplication1 = mat1.operator*(1.2);
	EXPECT_TRUE(multiplication1.get(0,0) == 4.8);
        EXPECT_TRUE(multiplication1.get(0,1) == 3.6);
	**/

	multiplication1 = mat1.operator*(3);
        EXPECT_TRUE(multiplication1.get(0,0) == 12.0);
        EXPECT_TRUE(multiplication1.get(0,1) == 9.0);

	multiplication1 = mat1.operator*(0);
        EXPECT_TRUE(multiplication1.get(0,0) == 0.0);
        EXPECT_TRUE(multiplication1.get(0,1) == 0.0);

	multiplication2 = mat2.operator*(-3);
	EXPECT_TRUE(multiplication2.get(0,0) == -12.0);
        EXPECT_TRUE(multiplication2.get(1,0) == -9.0);

	multiplication2 = mat2.operator*(-0);
        EXPECT_TRUE(multiplication2.get(0,0) == 0.0);
        EXPECT_TRUE(multiplication2.get(1,0) == 0.0);
}


TEST(Matrix, MatrixxMatrix_TwoSide)
{
	// 1x3 x 3x1
	
	// Udělání matic
	// Funkce si dokáže změnit velikost matice
        Matrix mat1(1,3), mat2(3,1), multiplication1, multiplication2(3,3);

        // Vyplnění matic
        EXPECT_TRUE(mat1.set(0,0,1));
        EXPECT_TRUE(mat1.set(0,1,-2));
        EXPECT_TRUE(mat1.set(0,2,2));

        EXPECT_TRUE(mat2.set(0,0,1));
        EXPECT_TRUE(mat2.set(1,0,-2));
        EXPECT_TRUE(mat2.set(2,0,3));

	// Jedna hodnota
	EXPECT_NO_THROW(multiplication1 = mat1.operator*(mat2));
        EXPECT_TRUE(multiplication1.get(0,0) == 11.0);

	EXPECT_NO_THROW(multiplication2 = mat2.operator*(mat1));
        EXPECT_TRUE(multiplication2.get(0,0) == 1.0);
        EXPECT_TRUE(multiplication2.get(0,1) == -2.0);
        EXPECT_TRUE(multiplication2.get(0,2) == 2.0);
	EXPECT_TRUE(multiplication2.get(1,0) == -2.0);
	EXPECT_TRUE(multiplication2.get(1,1) == 4.0);
	EXPECT_TRUE(multiplication2.get(1,2) == -4.0);
	EXPECT_TRUE(multiplication2.get(2,0) == 3.0);
	EXPECT_TRUE(multiplication2.get(2,1) == -6.0);
	EXPECT_TRUE(multiplication2.get(2,2) == 6.0);
}

TEST(Matrix, MatrixxMatrix_OneSide)
{
	// 1x2 3x1
        Matrix mat1(1,2), mat2(3,1), multiplication(3,2);

        // Vyplnění matic
        EXPECT_TRUE(mat1.set(0,0,3));
        EXPECT_TRUE(mat1.set(0,1,-2));

        EXPECT_TRUE(mat2.set(0,0,1));
        EXPECT_TRUE(mat2.set(1,0,-2));
        EXPECT_TRUE(mat2.set(2,0,3));

        // Mělo by jít jen z jedné strany
	EXPECT_ANY_THROW(multiplication = mat1.operator*(mat2));
	
	EXPECT_NO_THROW(multiplication = mat2.operator*(mat1));
        EXPECT_TRUE(multiplication.get(0,0) == 3.0);
        EXPECT_TRUE(multiplication.get(0,1) == -2.0);
        EXPECT_TRUE(multiplication.get(1,0) == -6.0);
        EXPECT_TRUE(multiplication.get(1,1) == 4.0);
        EXPECT_TRUE(multiplication.get(2,0) == 9.0);
        EXPECT_TRUE(multiplication.get(2,1) == -6.0);
}

TEST(Matrix, SolveEquation_BadDimensions)
{
	// 3x1 a 3x2 a 1x3 a 2x3 nejde
	Matrix mat1(3,1), mat2(3,2), mat3(1,3), mat4(2,3);
	std::vector<double> rightSide, result;

	// Vyplnění matic
        EXPECT_TRUE(mat1.set(0,0,1));
        EXPECT_TRUE(mat1.set(1,0,-2));
        EXPECT_TRUE(mat1.set(2,0,3));

	EXPECT_TRUE(mat2.set(0,0,1));
        EXPECT_TRUE(mat2.set(0,1,-2));
        EXPECT_TRUE(mat2.set(1,0,3));
	EXPECT_TRUE(mat2.set(1,1,2));
        EXPECT_TRUE(mat2.set(2,0,-4));
	EXPECT_TRUE(mat2.set(2,1,4));

	EXPECT_TRUE(mat3.set(0,0,1));
        EXPECT_TRUE(mat3.set(0,1,-1));
        EXPECT_TRUE(mat3.set(0,2,0));

	EXPECT_TRUE(mat4.set(0,0,1));
        EXPECT_TRUE(mat4.set(0,1,-2));
        EXPECT_TRUE(mat4.set(0,2,3));
        EXPECT_TRUE(mat4.set(1,0,2));
        EXPECT_TRUE(mat4.set(1,1,-4));
        EXPECT_TRUE(mat4.set(1,2,4));


	// Otestování
	EXPECT_ANY_THROW(result = mat1.solveEquation(rightSide));
	EXPECT_ANY_THROW(result = mat2.solveEquation(rightSide));
	EXPECT_ANY_THROW(result = mat3.solveEquation(rightSide));
	EXPECT_ANY_THROW(result = mat4.solveEquation(rightSide));
}

TEST(Matrix, SolveEquation_Singular)
{
	// Test singulární matice
	//3x3
	Matrix mat1(3,3);
        std::vector<double> rightSide, result;

	// vyplnění
	EXPECT_TRUE(mat1.set(0,0,2));
        EXPECT_TRUE(mat1.set(0,1,3));
        EXPECT_TRUE(mat1.set(0,2,4));
        EXPECT_TRUE(mat1.set(1,0,3));
        EXPECT_TRUE(mat1.set(1,1,4));
        EXPECT_TRUE(mat1.set(1,2,2));
	EXPECT_TRUE(mat1.set(2,0,2));
        EXPECT_TRUE(mat1.set(2,1,3));
	EXPECT_TRUE(mat1.set(2,2,4));

	rightSide.push_back(29);
	rightSide.push_back(26);
	rightSide.push_back(26);

	// chyba matice je singulární
	EXPECT_ANY_THROW(result = mat1.solveEquation(rightSide));
}

TEST(Matrix, SolveEquation_Calculation)
{
	//3x3
	Matrix mat1(3,3);
        std::vector<double> rightSide, result;

	// vyplnění
	EXPECT_TRUE(mat1.set(0,0,2));
        EXPECT_TRUE(mat1.set(0,1,3));
        EXPECT_TRUE(mat1.set(0,2,4));
        EXPECT_TRUE(mat1.set(1,0,3));
        EXPECT_TRUE(mat1.set(1,1,4));
        EXPECT_TRUE(mat1.set(1,2,2));
	EXPECT_TRUE(mat1.set(2,0,4));
        EXPECT_TRUE(mat1.set(2,1,2));
	EXPECT_TRUE(mat1.set(2,2,3));

	rightSide.push_back(29);
	rightSide.push_back(26);
	rightSide.push_back(26);

	EXPECT_NO_THROW(result = mat1.solveEquation(rightSide));
	EXPECT_TRUE(result.size() == 3);
	EXPECT_TRUE(result[0] == 2);
        EXPECT_TRUE(result[1] == 3);
        EXPECT_TRUE(result[2] == 4);
	
	// 4x4
	Matrix mat2(4,4);

	EXPECT_TRUE(mat2.set(0,0,1));
        EXPECT_TRUE(mat2.set(0,1,1));
        EXPECT_TRUE(mat2.set(0,2,1));
	EXPECT_TRUE(mat2.set(0,3,1));
        EXPECT_TRUE(mat2.set(1,0,1));
        EXPECT_TRUE(mat2.set(1,1,1));
        EXPECT_TRUE(mat2.set(1,2,-1));
	EXPECT_TRUE(mat2.set(1,3,-1));
        EXPECT_TRUE(mat2.set(2,0,1));
        EXPECT_TRUE(mat2.set(2,1,-1));
        EXPECT_TRUE(mat2.set(2,2,1));
	EXPECT_TRUE(mat2.set(2,3,-1));
	EXPECT_TRUE(mat2.set(3,0,1));
        EXPECT_TRUE(mat2.set(3,1,-1));
        EXPECT_TRUE(mat2.set(3,2,-1));
        EXPECT_TRUE(mat2.set(3,3,1));

	rightSide.pop_back();
	rightSide.pop_back();
	rightSide.pop_back();
	rightSide.push_back(10);
	rightSide.push_back(4);
	rightSide.push_back(2);
	rightSide.push_back(0);

	EXPECT_NO_THROW(result = mat2.solveEquation(rightSide));
        EXPECT_TRUE(result.size() == 4);
	EXPECT_TRUE(result[0] == 4);
	EXPECT_TRUE(result[1] == 3);
	EXPECT_TRUE(result[2] == 2);
	EXPECT_TRUE(result[3] == 1);
}


/*** Konec souboru white_box_tests.cpp ***/
