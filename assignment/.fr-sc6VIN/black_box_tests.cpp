//======== Copyright (c) 2019, FIT VUT Brno, All rights reserved. ============//
//
// Purpose:     Red-Black Tree - public interface tests
//
// $NoKeywords: $ivs_project_1 $black_box_tests.cpp
// $Author:     Matej Kudera <xkuder04@stud.fit.vutbr.cz>
// $Date:       $2019-02-12
//============================================================================//
/**
 * @file black_box_tests.cpp
 * @author Matej Kudera
 * 
 * @brief Implementace testu binarniho stromu.
 */

#include <vector>

#include "gtest/gtest.h"

#include "red_black_tree.h"

//============================================================================//
// ** ZDE DOPLNTE TESTY **
//
// Zde doplnte testy Red-Black Tree, testujte nasledujici:
// 1. Verejne rozhrani stromu
//    - InsertNode/DeleteNode a FindNode
//    - Chovani techto metod testuje pro prazdny i neprazdny strom.
// 2. Axiomy (tedy vzdy platne vlastnosti) Red-Black Tree:
//    - Vsechny listove uzly stromu jsou *VZDY* cerne.
//    - Kazdy cerveny uzel muze mit *POUZE* cerne potomky.
//    - Vsechny cesty od kazdeho listoveho uzlu ke koreni stromu obsahuji
//      *STEJNY* pocet cernych uzlu.
//============================================================================//

// EmptyTree
TEST(EmptyTree, InsertNode)
{
	BinaryTree tree;
	EXPECT_TRUE(tree.GetRoot() == NULL);
	
	std::pair<bool, BinaryTree::Node_t *> instertReturn;

	// pro node atributy 
	BinaryTree::Node_t *pointer1;
	BinaryTree::Node_t *pointer2;

	// přidání node, kontrola insert return, kontrola vnitřku node podle .h souboru
	instertReturn = tree.InsertNode(13);
	ASSERT_TRUE(tree.GetRoot() != NULL);
	ASSERT_TRUE(instertReturn.first == true);
        ASSERT_TRUE(instertReturn.second != NULL);
	EXPECT_TRUE(tree.GetRoot()->pParent == NULL);
	ASSERT_TRUE(tree.GetRoot()->pLeft != NULL);
	// Leaf
	EXPECT_TRUE(tree.GetRoot()->pLeft->pLeft == NULL);
	EXPECT_TRUE(tree.GetRoot()->pLeft->pRight == NULL);
	//
	ASSERT_TRUE(tree.GetRoot()->pRight != NULL);
	// Leaf
	EXPECT_TRUE(tree.GetRoot()->pRight->pLeft == NULL);
        EXPECT_TRUE(tree.GetRoot()->pRight->pRight == NULL);
	//
	EXPECT_TRUE(tree.GetRoot()->key == 13);
	pointer1 = instertReturn.second;

	// přidání node, kontrola insert return, kontrola vnitřku node podle .h souboru
	instertReturn = tree.InsertNode(69);
	pointer2 = instertReturn.second;
	ASSERT_TRUE(tree.GetRoot() != NULL);
	ASSERT_TRUE(instertReturn.first == true);
        ASSERT_TRUE(instertReturn.second != NULL);
	EXPECT_TRUE(pointer2->pParent != NULL);
	ASSERT_TRUE(pointer2->pLeft != NULL);
	// Leaf
	EXPECT_TRUE(pointer2->pLeft->pLeft == NULL);
	EXPECT_TRUE(pointer2->pLeft->pRight == NULL);
	//
	ASSERT_TRUE(pointer2->pRight != NULL);
	// Leaf
	EXPECT_TRUE(pointer2->pRight->pLeft == NULL);
        EXPECT_TRUE(pointer2->pRight->pRight == NULL);
	//
	EXPECT_TRUE(pointer2->key == 69);

	// kontrola jestly byl přidaný druhý node
	EXPECT_TRUE(pointer1->pParent == NULL);
	EXPECT_TRUE((pointer1->pLeft->pLeft != NULL && pointer1->pLeft->pRight != NULL) || (pointer1->pRight->pRight != NULL && pointer1->pRight->pLeft != NULL));
	EXPECT_TRUE(pointer1->key == 13);

	// kontrola jestli to vrátí dobrý pointer když tam vložíme špatný prvek
	instertReturn = tree.InsertNode(13);
	ASSERT_TRUE(tree.GetRoot() != NULL);
        EXPECT_TRUE(instertReturn.first == false);
        EXPECT_TRUE(instertReturn.second == pointer1);
}

TEST(EmptyTree, DeleteNode)
{
	BinaryTree tree;
        EXPECT_TRUE(tree.GetRoot() == NULL);

	EXPECT_FALSE(tree.DeleteNode(0));
	EXPECT_FALSE(tree.DeleteNode(13));
	EXPECT_FALSE(tree.DeleteNode(69));
}

TEST(EmptyTree, FindNode)
{
	BinaryTree tree;
        EXPECT_TRUE(tree.GetRoot() == NULL);

	EXPECT_TRUE(tree.FindNode(0) == NULL);
	EXPECT_TRUE(tree.FindNode(13) == NULL);
	EXPECT_TRUE(tree.FindNode(69) == NULL);
}

// Non-empty tree
TEST(NonEmptyTree, InsertNode_OneNodeTree)
{
	// vytvoření neprázdného stromu
	int inserted = 5;
	BinaryTree tree;
	std::pair<bool, BinaryTree::Node_t *> instertReturn;

	instertReturn = tree.InsertNode(inserted);
        EXPECT_TRUE(instertReturn.first == true);
	EXPECT_TRUE(instertReturn.second != NULL);

	// Kontrola kořene
	ASSERT_TRUE(tree.GetRoot() != NULL);
	EXPECT_TRUE(tree.GetRoot()->pParent == NULL);
	ASSERT_TRUE(instertReturn.second->pLeft != NULL);
	// Leaf
	EXPECT_TRUE(instertReturn.second->pLeft->pLeft == NULL);
	EXPECT_TRUE(instertReturn.second->pLeft->pRight == NULL);
	//
	ASSERT_TRUE(instertReturn.second->pRight != NULL);
	// Leaf
	EXPECT_TRUE(instertReturn.second->pRight->pLeft == NULL);
        EXPECT_TRUE(instertReturn.second->pRight->pRight == NULL);
	//
	EXPECT_TRUE(instertReturn.second->key == inserted);

	// pro test node atributů
	BinaryTree::Node_t *pointer1;

	// kontrola jestli je node ve stromě
	instertReturn = tree.InsertNode(inserted);
        ASSERT_TRUE(tree.GetRoot() != NULL);
        EXPECT_TRUE(instertReturn.first == false);

	// Přidání node a kontrola pointeru když vkládám už vložený node
	instertReturn = tree.InsertNode(100);
	ASSERT_TRUE(tree.GetRoot() != NULL);
	ASSERT_TRUE(instertReturn.first == true);
        ASSERT_TRUE(instertReturn.second != NULL);
	EXPECT_TRUE(instertReturn.second->pParent != NULL);
	ASSERT_TRUE(instertReturn.second->pLeft != NULL);
	// Leaf
	EXPECT_TRUE(instertReturn.second->pLeft->pLeft == NULL);
	EXPECT_TRUE(instertReturn.second->pLeft->pRight == NULL);
	//
	ASSERT_TRUE(instertReturn.second->pRight != NULL);
	// Leaf
	EXPECT_TRUE(instertReturn.second->pRight->pLeft == NULL);
        EXPECT_TRUE(instertReturn.second->pRight->pRight == NULL);
	//
	EXPECT_TRUE(instertReturn.second->key == 100);
	pointer1 = instertReturn.second;

	instertReturn = tree.InsertNode(100);
	ASSERT_TRUE(tree.GetRoot() != NULL);
        EXPECT_TRUE(instertReturn.first == false);
        EXPECT_TRUE(instertReturn.second == pointer1);
}

TEST(NonEmptyTree, InsertNode_MoreThenOneNodeTree)
{	
	// vytvoření neprázdného stromu
	BinaryTree tree;
	EXPECT_TRUE(tree.GetRoot() == NULL);
	std::pair<bool, BinaryTree::Node_t *> instertReturn;
	int values[] = { 5, 13, 15, 20, 30, 42, 50, 55, 60, 69, 70, 80, 85, 98 };
	for(int i = 0; i < sizeof(values)/sizeof(*values); i++)
    	{
		instertReturn = tree.InsertNode(values[i]);
        	ASSERT_TRUE(tree.GetRoot() != NULL);
        	EXPECT_TRUE(instertReturn.first == true);
		EXPECT_TRUE(instertReturn.second != NULL);
    	}

	// Kontrola kořene
	ASSERT_TRUE(tree.GetRoot() != NULL);
	EXPECT_TRUE(tree.GetRoot()->pParent == NULL);
	ASSERT_TRUE(tree.GetRoot()->pLeft != NULL);
	ASSERT_TRUE(tree.GetRoot()->pRight != NULL);
	EXPECT_TRUE((tree.GetRoot()->pLeft->pLeft != NULL && tree.GetRoot()->pLeft->pRight != NULL) || (tree.GetRoot()->pRight->pRight != NULL && tree.GetRoot()->pRight->pLeft != NULL));

	// To test node atributes 
	BinaryTree::Node_t *pointer1;

	// Kontrola atributů posledního vloženího nodu
	ASSERT_TRUE(tree.GetRoot() != NULL);
	EXPECT_TRUE(instertReturn.second->pParent != NULL);
	ASSERT_TRUE(instertReturn.second->pLeft != NULL);
	// Leaf
	EXPECT_TRUE(instertReturn.second->pLeft->pLeft == NULL);
	EXPECT_TRUE(instertReturn.second->pLeft->pRight == NULL);
	//
	ASSERT_TRUE(instertReturn.second->pRight != NULL);
	// Leaf
	EXPECT_TRUE(instertReturn.second->pRight->pLeft == NULL);
        EXPECT_TRUE(instertReturn.second->pRight->pRight == NULL);
	//
	EXPECT_TRUE(instertReturn.second->key == values[sizeof(values) / sizeof(*values) - 1]);

	// Kontrola jestli je už node ve stromě
	instertReturn = tree.InsertNode(values[sizeof(values) / sizeof(*values) - 1]);
        ASSERT_TRUE(tree.GetRoot() != NULL);
        EXPECT_TRUE(instertReturn.first == false);

	// Přidání node a kontrola pointeru když vkládám už vložený node
	instertReturn = tree.InsertNode(100);
	ASSERT_TRUE(tree.GetRoot() != NULL);
	ASSERT_TRUE(instertReturn.first == true);
        ASSERT_TRUE(instertReturn.second != NULL);
	EXPECT_TRUE(instertReturn.second->pParent != NULL);
	EXPECT_TRUE(instertReturn.second->pLeft != NULL);
	EXPECT_TRUE(instertReturn.second->pRight != NULL);
	EXPECT_TRUE(instertReturn.second->key == 100);
	pointer1 = instertReturn.second;

	instertReturn = tree.InsertNode(100);
	ASSERT_TRUE(tree.GetRoot() != NULL);
        EXPECT_TRUE(instertReturn.first == false);
        EXPECT_TRUE(instertReturn.second == pointer1);
}

TEST(NonEmptyTree, DeleteNode)
{
	// Vytvořené neprázdného stromu
	BinaryTree tree;
	EXPECT_TRUE(tree.GetRoot() == NULL);
	std::pair<bool, BinaryTree::Node_t *> instertReturn;
	int values[] = { 5, 13, 15, 20, 30, 42, 50, 55, 60, 69, 70, 80, 85, 98 };
	for(int i = 0; i < sizeof(values)/sizeof(*values); i++)
    	{
		instertReturn = tree.InsertNode(values[i]);
        	ASSERT_TRUE(tree.GetRoot() != NULL);
        	EXPECT_TRUE(instertReturn.first == true);
		EXPECT_TRUE(instertReturn.second != NULL);
    	}

	EXPECT_TRUE(tree.DeleteNode(5));
	EXPECT_TRUE(tree.DeleteNode(20));
	EXPECT_TRUE(tree.DeleteNode(55));
	EXPECT_TRUE(tree.DeleteNode(70));
	EXPECT_TRUE(tree.DeleteNode(98));

	EXPECT_FALSE(tree.DeleteNode(100));
	EXPECT_FALSE(tree.DeleteNode(20));
	EXPECT_FALSE(tree.DeleteNode(-5));

}

TEST(NonEmptyTree, FindNode)
{
	// Vytvořené neprázdného stromu
	BinaryTree tree;
	EXPECT_TRUE(tree.GetRoot() == NULL);
	std::pair<bool, BinaryTree::Node_t *> instertReturn;
	int values[] = { 5, 13, 15, 20, 30, 42, 50, 55, 60, 69, 70, 80, 85, 98 };
	for(int i = 0; i < sizeof(values)/sizeof(*values); i++)
    	{
		instertReturn = tree.InsertNode(values[i]);
        	ASSERT_TRUE(tree.GetRoot() != NULL);
        	EXPECT_TRUE(instertReturn.first == true);
		EXPECT_TRUE(instertReturn.second != NULL);
    	}
	
	EXPECT_TRUE(tree.FindNode(5) != NULL);
	EXPECT_TRUE(tree.FindNode(55) != NULL);
	EXPECT_TRUE(tree.FindNode(98) != NULL);

	EXPECT_FALSE(tree.FindNode(100) != NULL);
	EXPECT_FALSE(tree.FindNode(-5) != NULL);

	EXPECT_TRUE(tree.FindNode(98) == tree.FindNode(98));
}

// Axiomy
TEST(TreeAxioms, Axiom1_EmptyTree)
{
	BinaryTree tree;
        EXPECT_TRUE(tree.GetRoot() == NULL);
	
	// prázdný pole
	std::vector<BinaryTree::Node_t*> outLeafNodes;
	tree.GetLeafNodes(outLeafNodes);
	
	EXPECT_TRUE(outLeafNodes.empty());
	
	// kontrola jestli se pole čistí
	outLeafNodes.push_back(NULL);
	outLeafNodes.push_back(NULL);
	
	tree.GetLeafNodes(outLeafNodes);
	EXPECT_TRUE(outLeafNodes.empty());
}

TEST(TreeAxioms, Axiom1_NonEmptyTree)
{
	// udělání neprázdného stromu
	BinaryTree tree;
	EXPECT_TRUE(tree.GetRoot() == NULL);
	std::pair<bool, BinaryTree::Node_t *> instertReturn;
	int values[] = { 5, 13, 15, 20, 30, 42, 50, 55, 60, 69, 70, 80, 85, 98 };
	for(int i = 0; i < sizeof(values)/sizeof(*values); i++)
    	{
		instertReturn = tree.InsertNode(values[i]);
        	ASSERT_TRUE(tree.GetRoot() != NULL);
        	EXPECT_TRUE(instertReturn.first == true);
		EXPECT_TRUE(instertReturn.second != NULL);
    	}

	// prázdné pole
	std::vector<BinaryTree::Node_t*> outLeafNodes, compare;
        tree.GetLeafNodes(outLeafNodes);
	tree.GetLeafNodes(compare);

	for (int i = 0; i < outLeafNodes.size(); i++)
  	{
		// Kontrola jestli je to list
		EXPECT_TRUE(outLeafNodes[i]->pLeft == NULL);
		EXPECT_TRUE(outLeafNodes[i]->pRight == NULL);
		
		// kontrola barvy listu
		EXPECT_TRUE(outLeafNodes[i]->color == BLACK);
  	}

	// Kontrola čištění pole
	outLeafNodes.push_back(NULL);
	tree.GetLeafNodes(outLeafNodes);
	
	for (int i = 0; i < outLeafNodes.size(); i++)
  	{
		// Kontrola jestli je to list
		EXPECT_TRUE(outLeafNodes[i]->pLeft == NULL);
		EXPECT_TRUE(outLeafNodes[i]->pRight == NULL);
		
		// kontrola barvy listu
		EXPECT_TRUE(outLeafNodes[i]->color == BLACK);
  	}

	EXPECT_TRUE(outLeafNodes == compare);
}

TEST(TreeAxioms, Axiom2_EmptyTree)
{
	BinaryTree tree;
        EXPECT_TRUE(tree.GetRoot() == NULL);

	// prázdné pole
	std::vector<BinaryTree::Node_t*> outAllNodes;
        tree.GetAllNodes(outAllNodes);

	EXPECT_TRUE(outAllNodes.empty());
	
	// Kontrola čištění pole
	outAllNodes.push_back(NULL);
	outAllNodes.push_back(NULL);
	
	tree.GetAllNodes(outAllNodes);
	EXPECT_TRUE(outAllNodes.empty());
}

TEST(TreeAxioms, Axiom2_NonEmptyTree)
{
	// vytvoření neprázdného stromu
	BinaryTree tree;
	EXPECT_TRUE(tree.GetRoot() == NULL);
	std::pair<bool, BinaryTree::Node_t *> instertReturn;
	int values[] = { 5, 13, 15, 20, 30, 42, 50, 55, 60, 69, 70, 80, 85, 98 };
	for(int i = 0; i < sizeof(values)/sizeof(*values); i++)
    	{
		instertReturn = tree.InsertNode(values[i]);
        	ASSERT_TRUE(tree.GetRoot() != NULL);
        	EXPECT_TRUE(instertReturn.first == true);
		EXPECT_TRUE(instertReturn.second != NULL);
    	}

	// prázdné pole
	std::vector<BinaryTree::Node_t*> outAllNodes, compare;
	tree.GetAllNodes(outAllNodes);
	tree.GetAllNodes(compare);

	for (int i = 0; i < outAllNodes.size(); i++)
  	{
		// kontrola jestli je node RED, pak potomci jsou BLACK
		if (outAllNodes[i]->color == RED)
		{
			EXPECT_TRUE(outAllNodes[i]->pLeft->color == BLACK);
			EXPECT_TRUE(outAllNodes[i]->pRight->color == BLACK);
		}
  	}

	// Kontrola čištění pole
	outAllNodes.push_back(NULL);
	tree.GetAllNodes(outAllNodes);

	for (int i = 0; i < outAllNodes.size(); i++)
  	{
		// kontrola jestli je node RED, pak potomci jsou BLACK
		if (outAllNodes[i]->color == RED)
		{
			EXPECT_TRUE(outAllNodes[i]->pLeft->color == BLACK);
			EXPECT_TRUE(outAllNodes[i]->pRight->color == BLACK);
		}
  	}

	EXPECT_TRUE(outAllNodes == compare);
}


TEST(TreeAxioms, Axiom3_EmptyTree)
{
	BinaryTree tree;
        EXPECT_TRUE(tree.GetRoot() == NULL);
	
	// prázdné pole
	std::vector<BinaryTree::Node_t*> outLeafNodes;
	tree.GetLeafNodes(outLeafNodes);
	
	EXPECT_TRUE(outLeafNodes.empty());
	
	// kontrola čištění pole
	outLeafNodes.push_back(NULL);
	outLeafNodes.push_back(NULL);
	
	tree.GetLeafNodes(outLeafNodes);
	EXPECT_TRUE(outLeafNodes.empty());
}

TEST(TreeAxioms, Axiom3_NonEmptyTree)
{
	// vytvoření neprázdného pole
	BinaryTree tree;
	EXPECT_TRUE(tree.GetRoot() == NULL);
	std::pair<bool, BinaryTree::Node_t *> instertReturn;
	int values[] = { 5, 13, 15, 20, 30, 42, 50, 55, 60, 69, 70, 80, 85, 98 };
	for(int i = 0; i < sizeof(values)/sizeof(*values); i++)
    	{
		instertReturn = tree.InsertNode(values[i]);
        	ASSERT_TRUE(tree.GetRoot() != NULL);
        	EXPECT_TRUE(instertReturn.first == true);
		EXPECT_TRUE(instertReturn.second != NULL);
    	}

	// prázdné pole
	std::vector<BinaryTree::Node_t*> outLeafNodes, compare;
        tree.GetLeafNodes(outLeafNodes);
	tree.GetLeafNodes(compare);

	int blackNodesCount = 0;

	for (int i = 0; i < outLeafNodes.size(); i++)
  	{
		// Kontrola jestli je stejně černých nodů ke kořeni od každího listu
		int nodesCount = 0;
		BinaryTree::Node_t *currentNode = outLeafNodes[i]->pParent;

		while (currentNode != NULL)
		{
			if (currentNode->color == BLACK)
			{
				nodesCount++;
			}
			
			currentNode = currentNode->pParent;
		}

		if (blackNodesCount == 0)
		{
			blackNodesCount = nodesCount;
		}

		EXPECT_TRUE(blackNodesCount == nodesCount);
  	}

	// kontrola čištění pole
	outLeafNodes.push_back(NULL);
	tree.GetLeafNodes(outLeafNodes);
	blackNodesCount = 0;

	for (int i = 0; i < outLeafNodes.size(); i++)
  	{
		// Kontrola jestli je stejně černých nodů ke kořeni od každího listu
		int nodesCount = 0;
		BinaryTree::Node_t *currentNode = outLeafNodes[i]->pParent;

		while (currentNode != NULL)
		{
			if (currentNode->color == BLACK)
			{
				nodesCount++;
			}
			
			currentNode = currentNode->pParent;
		}

		if (blackNodesCount == 0)
		{
			blackNodesCount = nodesCount;
		}

		EXPECT_TRUE(blackNodesCount == nodesCount);
  	}


	EXPECT_TRUE(outLeafNodes == compare);
}


/*** Konec souboru black_box_tests.cpp ***/
